﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using semente.Models;
using semente.DTO;
using System.Linq.Expressions;

namespace semente.Controllers
{
    [Produces("application/json")]
    [Route("api/LinhasReceita")]
    public class LinhasReceitaController : Controller
    {
        private readonly sementeContext _context;

        public LinhasReceitaController(sementeContext context)
        {
            _context = context;
        }

        // GET: api/LinhasReceita
        [HttpGet]
        public IEnumerable<LinhaReceitaDTO> GetLinhaReceita()
        {
            return _context.LinhaReceita
                    .Include(lr => lr.Receita)
                    .Select(asLinhaReceitaDTO);
        }

        // GET: api/LinhasReceita/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLinhaReceita([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linhaReceita = await _context.LinhaReceita.SingleOrDefaultAsync(m => m.LinhaReceitaID == id);

            if (linhaReceita == null)
            {
                return NotFound();
            }

            return Ok(linhaReceita);
        }

        // PUT: api/LinhasReceita/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLinhaReceita([FromRoute] int id, [FromBody] LinhaReceita linhaReceita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != linhaReceita.LinhaReceitaID)
            {
                return BadRequest();
            }

            _context.Entry(linhaReceita).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LinhaReceitaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/LinhasReceita
        [HttpPost]
        public async Task<IActionResult> PostLinhaReceita([FromBody] LinhaReceita linhaReceita)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.LinhaReceita.Add(linhaReceita);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetLinhaReceita", new { id = linhaReceita.LinhaReceitaID }, linhaReceita);
        }

        // DELETE: api/LinhasReceita/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLinhaReceita([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var linhaReceita = await _context.LinhaReceita.SingleOrDefaultAsync(m => m.LinhaReceitaID == id);
            if (linhaReceita == null)
            {
                return NotFound();
            }

            _context.LinhaReceita.Remove(linhaReceita);
            await _context.SaveChangesAsync();

            return Ok(linhaReceita);
        }

        private bool LinhaReceitaExists(int id)
        {
            return _context.LinhaReceita.Any(e => e.LinhaReceitaID == id);
        }

        private static readonly Expression<Func<LinhaReceita, LinhaReceitaDTO>> asLinhaReceitaDTO = x => new LinhaReceitaDTO
        {
            quantidade = x.Quantidade,
            validade = x.Validade,
            dataReceita = x.Receita.Data
        };
    }
}