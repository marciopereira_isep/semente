﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace semente.Models
{
    public class Receita
    {
        public int ReceitaID { get; set; }
        public DateTime Data { get; set; }
    }
}
