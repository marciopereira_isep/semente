﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace semente.Models
{
    public class LinhaReceita
    {
        public int LinhaReceitaID { get; set; }
        public int Quantidade { get; set; }
        public DateTime Validade { get; set; }
        public int ReceitaId { get; set; }
        public Receita Receita { get; set; }
    }
}
