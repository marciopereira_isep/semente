﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace semente.Models
{
    public class Medicamento
    {
        public int MedicamentoId { get; set; }

        public string Nome { get; set; }
    }
}
