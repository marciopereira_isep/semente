﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using semente.Models;

namespace semente.Models
{
    public class sementeContext : DbContext
    {
        public sementeContext (DbContextOptions<sementeContext> options)
            : base(options)
        {
        }

        public DbSet<semente.Models.Medicamento> Medicamento { get; set; }

        public DbSet<semente.Models.LinhaReceita> LinhaReceita { get; set; }

        public DbSet<semente.Models.Receita> Receita { get; set; }
    }
}
