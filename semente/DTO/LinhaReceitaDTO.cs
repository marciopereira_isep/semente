﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace semente.DTO
{
    public class LinhaReceitaDTO
    {
        public int quantidade { get; set; }
        public DateTime validade { get; set; }
        public DateTime dataReceita { get; set; }
    }
}
